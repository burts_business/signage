<?php get_header(); /**
 * Template Name: Home Page
 */?>
    
    <section class="section clear">
	    
	    <h1><?php the_field('tag_line'); ?></h1>
	     <div class="copy clear">
	    	<?php the_field('intro_copy'); ?>
	    	<a href="#contact">
		    	<div class="button">
		    		Get In Touch
	    		</div>
	    	</a>
	    </div>
	    
    </section>
    
    <section class="section clear">
	    <h3>Services</h3>
	    <a href="<?php echo home_url(); ?>/frosting/">
		    <div class="third">
			    <h2>Frosting</h2>
			    <img src="<?php the_field('frosting_image'); ?>" alt="<?php echo $image['alt']; ?>" />
		    </div>
	    </a>
	    <a href="<?php echo home_url(); ?>/vinyl-signage/">
		    <div class="third">
			    <h2>Vinyl Signage</h2>
			    <img src="<?php the_field('decals'); ?>" alt="<?php echo $image['alt']; ?>" />
		    </div>
	    </a>
	    <a href="<?php echo home_url(); ?>/custom-jobs/">
		    <div class="third">
			    <h2>Custom Jobs</h2>
			    <img src="<?php the_field('custom'); ?>" alt="<?php echo $image['alt']; ?>" />
		    </div>
	    </a>
	   
    </section>
    
    <section class="section clear" style="padding-top: 0px;">
	    
	    <?php 
			$images = get_field('gallery');
			
			if( $images ): ?>
			    <ul class="gallery">
			        <?php foreach( $images as $image ): ?>
			            <li>
			               <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			            </li>
			        <?php endforeach; ?>
			    </ul>
			<?php endif; ?>
	    
	     <img class="van" src="<?php bloginfo('stylesheet_directory'); ?>/images/van-11.svg" />

    </section>

    
    <section id="contact" class="section clear">
	    <h2>Get in touch with us</h2>
	    <div class="copy">
	    	<p>If you need any frosting, vinyl Graphics, custom signage or have a general enquiry call or email us now. With any email or voicemail enquires we will respond within 24 hours.</p>
		</div>
	    <div class="half">
	    	<h2>Phone:</h2>
	    	<p>+64 22 129 0161</p>
	    </div>
	    <div class="half">
	    	<h2>Email:</h2>
	    	<p><a href="mailto:sales@blinkltd.co.nz">sales@blinkltd.co.nz</a></p>
	    </div>
	    <div class="contact-form">
		    <?php echo do_shortcode('[contact-form-7 id="39" title="Signage"]'); ?>
	    </div>
    </section>
       
    
<?php get_footer(); ?>