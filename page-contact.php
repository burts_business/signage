<?php get_header(); /**
 * Template Name: Contact Page
 */?>
    
    <section id="contact" class="section clear">
	    <h2>Get in touch with us</h2>
	    <div class="copy">
	    	<p>If you need any frosting, vinyl Graphics, custom signage or have a general enquiry call or email us now. With any email or voicemail enquires we will respond within 24 hours.</p>
	    </div>
	    <div class="half">
	    	<h2>Phone:</h2>
	    	<p>+64 22 129 0161</p>
	    </div>
	    <div class="half">
	    	<h2>Email:</h2>
	    	<p><a href="mailto:sales@blinkltd.co.nz">sales@blinkltd.co.nz</a></p>
	    </div>
	    <div class="contact-form">
		    <?php echo do_shortcode('[contact-form-7 id="39" title="Signage"]'); ?>
	    </div>
    </section>   
       
    
<?php get_footer(); ?>