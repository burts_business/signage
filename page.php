<?php get_header(); /**
 * Template Name: Normal Page
 */
?>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>    
                   
    <section class="section clear">
	    
	    <h1><?php the_title(); ?></h1>
	    <div class="copy clear">
	    	<p><?php the_content(); ?></p>
	    	<a href="#contact">
		    	<div class="button">
		    		Get In Touch
	    		</div>
	    	</a>
	    </div>	    		
	
	    </div>

	    
    </section>
    
    
    
    <section class="section clear" style="padding-top: 0px;">
	   	    <?php 
			$images = get_field('gallery');
			
			if( $images ): ?>
			    <ul class="gallery">
			        <?php foreach( $images as $image ): ?>
			            <li>
			               <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			            </li>
			        <?php endforeach; ?>
			    </ul>
			<?php endif; ?>
	    

    </section>

    
    <section id="contact" class="section clear">
	    <h2>Get in touch with us</h2>
	    <div class="copy">
	    	<p>If you need any frosting, vinyl graphics, custom signage or have a general enquiry call or email us now. With any email or voicemail enquires we will respond within 24 hours.</p>
	    </div>
	    <div class="half">
	    	<h2>Phone:</h2>
	    	<p>+64 22 129 0161</p>
	    </div>
	    <div class="half">
	    	<h2>Email:</h2>
	    	<p><a href="mailto:sales@blinkltd.co.nz">sales@blinkltd.co.nz</a></p>
	    </div>
	    <div class="contact-form">
		    <?php echo do_shortcode('[contact-form-7 id="39" title="Signage"]'); ?>
	    </div>
    </section>

                   <?php endwhile; ?>
         <?php endif; ?>
    
<?php get_footer(); ?>