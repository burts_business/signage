<?php //Gets post cat slug and looks for single-[cat slug].php and applies it
add_filter('single_template', create_function(
	'$the_template',
	'foreach( (array) get_the_category() as $cat ) {
		if ( file_exists(STYLESHEETPATH . "/single-{$cat->slug}.php") )
		return STYLESHEETPATH . "/single-{$cat->slug}.php"; }
	return $the_template;' )
);
?>
<?php
add_theme_support( 'post-thumbnails' ); 
?>
<?php
/*
Plugin Name: Get Remote Image to Custom Field
*/
add_filter('really_simple_csv_importer_post_saved', function($post)
{
    if (is_object($post)) {
        // Get the meta data of which key is "image"
        $image = $post->image;
        // Create a instance of helper class
        $h = RSCSV_Import_Post_Helper::getByID($post->ID);
        // Get the remote image data
        $file = $h->remoteGet($image);
        // Then, attach it
        $attachment_id = $h->setAttachment($file);
        // Finally, replace the original data with the attachment id
        $h->setMeta( array( 'image' => $attachment_id ) );
    }
});
?>