
			
	
    <section id="footer" class="section clear">
	    <div class="third left">
		    <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Blink Signage" title="Blink Signage" /></a>
	    </div>
	    <div class="third right">
		    <p>&#169; Copyright Blink Ltd. <?php echo date("Y") ?></p>
			<a href="http://blinkltd.co.nz/" target="_blank">handmade by blink.</a>

	    </div>
    </section>
	        

			
	
	
	<a class="exit-off-canvas"></a>

  </div><!--innnerwrap-->
</div><!--off canvas wrap-->

	
	
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300' rel='stylesheet' type='text/css'>
	
	<script src="//cdn.rawgit.com/noelboss/featherlight/1.2.3/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
	



  
<?php wp_footer(); ?>


</body>
</html>