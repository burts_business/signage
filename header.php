<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Blink Signage</title>
    <!-- Thanks to Pure CSS parallax scroll demo #3 by Keith Clark for the Perspective Parallax --> 
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/typography.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/core.css" />
    
    <link type="text/css" rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/demo.css" />
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.mmenu.all.css" />
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.mmenu.positioning.css" />
	<link type="text/css" rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" />
	
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/media.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style.css" />
    <link href="//cdn.rawgit.com/noelboss/featherlight/1.3.2/release/featherlight.min.css" type="text/css" rel="stylesheet" title="Featherlight Styles" />
    
   

   	
   	<link rel="icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />
   	<link rel="shortcut icon" href="<?php bloginfo('siteurl'); ?>/favicon.ico" type="image/x-icon" />

 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
 	

	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.mmenu.min.all.js"></script>

 	<script type="text/javascript">
	//uncomment and change this to false if you're having trouble with WOFFs
	//var woffEnabled = true;
	//to place your webfonts in a custom directory 
	//uncomment this and set it to where your webfonts are.
	//var customPath = "<?php bloginfo('stylesheet_directory'); ?>/fonts";
	</script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/MyFontsWebfontsKit.js"></script>
  	
	<script type="text/javascript">
		jQuery(function($){ 

				$("#menu")
					.mmenu({
						 offCanvas: {
			               position  : "right",
			               zposition : "back"
			            }

					}).on( 'click',
						'a[href^="#/"]',
						function() {
							alert( "Thank you for clicking, but that's a demo link." );
							return false;
						}
					);
			});
			
					
			</script>
		
    <?php wp_head(); ?>
  </head>
  
  <body <?php body_class(); ?>>

  <div class="off-canvas-wrap">
	 <div class="inner-wrap"> 
		  
		
		<nav id="menu">
				<ul>
					<li><a href="<?php echo home_url(); ?>">Home</a></li>
					<li><a href="<?php echo home_url(); ?>/frosting/">Frosting</a></li>
					<li><a href="<?php echo home_url(); ?>/vinyl-signage/">Vinyl Signage</a></li>
					<li><a href="<?php echo home_url(); ?>/custom-jobs/">Custom Jobs</a></li>
					<li><a href="<?php echo home_url(); ?>/contact-2/">Contact</a></li>
					<li>
						<a href="http://facebook.com/blinksignage">
							<img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/facebook.svg" alt="Facebook - Blink" />
						</a>
						<a href="http://instagram.com/blinkboys">
							<img class="social" src="<?php bloginfo('stylesheet_directory'); ?>/images/instagram.svg" alt="Instagram - Blink" />
						</a>
					</li>
				</ul>
		</nav>
		

		<div id="main-content" class="clear" role="document">
			
			<?php if ( is_page( 'Homepage' ) ){ ?>
				
				<section id="intro" class="clear">
					<section class="clear">
						<header class="clear">	
							<a href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Blink Signage" title="Blink Signage" /></a>

						<div class="mobile right mob-logo">
							<a href="#menu"></a>
						</div>
				        	
					    </header>	
					    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/image-home.jpg" alt="blink boys" />			
					     
					</section>
									    
				</section>
			
			<?php }
				else 
			{?>
			
			<section>
				<header class="header-home clear">	
					<a href="<?php echo home_url(); ?>"><img class="home-logo" src="<?php bloginfo('stylesheet_directory'); ?>/images/logo.svg" alt="Blink Signage" title="Blink Signage" /></a>
		        	
						<div class="mobile right mob-logo">
							<a href="#menu"></a>
						</div>
					
					</nav> 
			    </header>	
			    
			    <?php 
					$image = get_field('header_image');
					if( !empty($image) ): ?>
						<img class="header-image" src="<?php the_field('header_image'); ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endif; ?>
			</section>
			  			
			<?php }
			?>
