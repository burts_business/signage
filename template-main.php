<?php /* Template Name: Main */
get_header(); ?>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>    

	<section id="post-<?php the_ID(); ?>" class="cd-section clear main ">
		<?php the_title('<h1>', '</h1>' );
		
		if ( has_post_thumbnail() ) {
		 the_post_thumbnail();
		 }?>
		<div class="lead clear">
			<?php the_content(); ?>
		</div>

		
		<?php if ( is_page( 'Our Work' ) ) {?>
			<?php	$images = get_field('gallery');
			if( $images ):
			
			foreach( $images as $image ): ?>

                <a href="<?php echo $image['url']; ?>" data-featherlight="image">
                     <img class="gallery-img" src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
		     
		        <?php endforeach; ?>
		    <?php endif; ?>
			 
			<?php } elseif ( is_page( 'FAQ' ) ) {?>
			  <div class="cd-faq-items">
				<ul id="basics" class="cd-faq-group">
				<?php if( have_rows('faq') ):
					while ( have_rows('faq') ) : the_row(); ?>
					<li>
						<a class="cd-faq-trigger" href="#0"><?php the_sub_field('title'); ?></a>
						<div class="cd-faq-content">
							<?php the_sub_field('faq_content'); ?>
						</div> <!-- cd-faq-content -->
					</li>
					<?php endwhile; ?>
					<?php else :
					endif; ?>
				</ul> <!-- cd-faq-group -->
			</div>
			
			<?php } else {
			  //everything else
		}?>
		
	
		
	</section>

		
	<?php if ( is_page( 'Contact Us' ) ){ ?>
			<div class="gallery clear">
			    <?php $images = get_field('gallery');
					if( $images ): ?>
				   	<?php foreach( $images as $image ): ?>
				    	<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
				    <?php endforeach; ?>
				<?php endif; ?>
				<div class="overlay">
				</div>
				<section class=" examples cd-section lead clear white">
			    	<h2>Examples of our work</h2>
			    	<p>Take a look at the discreet, professional installs will look like 
			in your home.</p>
			    	<a href="<?php echo home_url(); ?>/our-work/" class="button">Our work</a>
			    </section>
			</div>
		<?php }  else { ?>
			<div class="clear white">
				<section class="cd-section lead">
					<h2>It couldn't be easier</h2>
					<p>With an emphasis on quality and value for money, Attic Installations can solve your home storage problems today. So why wait?</p>
					<a href="mailto:sales@atticinstallations.co.nz" class="button">Get a free quote</a>
				</section>
			</div>	
		<?php }?>
          
    <?php endwhile; ?>
<?php endif; ?>
				
<?php get_footer(); ?>